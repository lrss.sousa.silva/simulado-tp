const express = require("express");
const bodyParser = require("body-parser");
const port = 8000;
const db = require("./db/connection");

let app = express();
app.use(bodyParser.json());

app.post("/localizacao", (req, res) => {
  const body = req.body;
  let sql = `INSERT INTO LOCALIZACAO (CEP, CIDADEUF, BAIRRO, LOCALIDADE, ATIVO) VALUES (?, ?, ?, ?, ?)`;
  db.query(sql, [body.cep, body.cidadeuf, body.bairro, body.localidade, 1], (err, result) => {
    if (err) {
      res.status(400).json({ message: err });
      return;
    }
    res.status(201).json({ message: "Localização salva com sucesso!" });
  });
});

app.put("/localizacao/:id", (req, res) => {
  const body = req.body;
  const id = req.params.id;

  let sql = `UPDATE LOCALIZACAO SET cep = ?, cidadeuf = ?, bairro = ?, localidade = ? WHERE id = ?`;
  let values = [body.cep, body.cidade, body.bairro, body.localidade, id];
  db.query(sql, values, (err, result) => {
    if (err) {
      res.status(400).json({ message: err });
      return;
    }
    res.status(201).json({ message: "Localização alterada com sucesso!" });
  });
});

app.delete("/localizacao/:id", (req, res) => {
  const id = req.params.id;

  let sql = `UPDATE LOCALIZACAO SET ATIVO = 0 WHERE id = ?`;
  db.query(sql, [id], (err, result) => {
    if (err) {
      res.status(400).json({ message: err });
      return;
    }
    res.status(201).json({ message: "Localização inativada com sucesso!" });
  });
});

app.get("/localizacao/cep/:cep", (req, res) => {
  const cep = Number(req.params.cep);

  let sql = `SELECT * FROM LOCALIZACAO WHERE CEP = ? AND ATIVO = 1`;
  db.query(sql, [cep], (err, result) => {
    if (err) {
      res.status(400).json({ message: err });
      return;
    }
    res.status(200).json(result);
  });
});

app.get("/localizacao/bairro/:bairro", (req, res) => {
  const bairro = req.params.bairro;

  let sql = `SELECT * FROM LOCALIZACAO WHERE BAIRRO = ? AND ATIVO = 1`;
  db.query(sql, [bairro], (err, result) => {
    if (err) {
      res.status(400).json({ message: err });
      return;
    }
    res.status(200).json(result);
  });
});

app.get("/localizacao/cidade", (req, res) => {
  const cidade = req.query.cidade;

  console.log(cidade);
  
  let sql = `SELECT * FROM LOCALIZACAO WHERE CIDADEUF = ? AND ATIVO = 1`;
  db.query(sql, [cidade], (err, result) => {
    if (err) {
      res.status(400).json({ message: err });
      return;
    }
    res.status(200).json(result);
  });
});

app.get("/localizacao/localidade", (req, res) => {
  const localidade = req.query.localidade;

  let sql = `SELECT * FROM LOCALIZACAO WHERE LOCALIDADE LIKE "%${localidade}%" AND ATIVO = 1`;
  db.query(sql, [localidade], (err, result) => {
    if (err) {
      res.status(400).json({ message: err });
      return;
    }
    res.status(200).json(result);
  });
});

app.listen(port, () => {
  console.log("Iniciando API na porta: " + port);
});
