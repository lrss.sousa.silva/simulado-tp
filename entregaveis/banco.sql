CREATE TABLE localizacao (
   id int(11) NOT NULL AUTO_INCREMENT,
   cep numeric(11),
   cidadeuf varchar(500), 
   bairro varchar(1500), 
   localidade varchar(500),
   ativo numeric(1),
   PRIMARY KEY (id)
);
   