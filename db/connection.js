const mysql = require('mysql2');

const conn = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'root',
    database: 'simulado'
})

conn.connect((error)=>{
    if(error){
        console.log(error);
        return;
    }
    console.log('conectado com sucesso')
})

module.exports = conn;